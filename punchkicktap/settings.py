import ast
import sys

import os

IS_TEST_MODE = len(sys.argv) > 1 and sys.argv[1] == 'test'

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'False').capitalize())

APPEND_SLASH = True
if DEBUG:
    ALLOWED_HOSTS = [
        '*'
    ]
else:
    ALLOWED_HOSTS = [
        'www.%s' % os.environ['SITE_DOMAIN'],
        'api.%s' % os.environ['SITE_DOMAIN'],
        os.environ['SITE_DOMAIN'],
        'of488brdk8.execute-api.us-east-1.amazonaws.com'
    ]
INTERNAL_IPS = [
    'localhost',
    'localhost:4497',
    '127.0.0.1',
    '0.0.0.0',
    '0.0.0.0:80',
    '10.0.2.2'
]

AUTH_USER_MODEL = 'users.User'

# Application definition

INSTALLED_APPS = [
    'punchkicktap.users',
    'punchkicktap.utils',

    'django_extensions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.sessions',
    'admin_honeypot',
    'zappa',
    'storages',
    'rest_framework',
    'rest_framework.authtoken',
]

MIDDLEWARE = [
    'django.middleware.cache.UpdateCacheMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'punchkicktap.middleware.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware'
]

ROOT_URLCONF = 'punchkicktap.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'punchkicktap/templates/')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'punchkicktap.context_processors.api_keys',
                'punchkicktap.context_processors.constants',
            ],
        },
    },
]

WSGI_APPLICATION = 'punchkicktap.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ['POSTGRES_NAME'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        'PORT': os.environ['POSTGRES_PORT'],
    }
}

# Cache
CACHE_MIDDLEWARE_ALIAS = 'site'
CACHE_MIDDLEWARE_SECONDS = 60 * 60 * 24
CACHE_BACKEND = os.environ['CACHE_BACKEND'] if not IS_TEST_MODE else 'django.core.cache.backends.locmem.LocMemCache'
url = ''
if CACHE_BACKEND == 'django_redis.cache.RedisCache':
    url = 'redis://%s:%s' % (
        os.environ['REDIS_HOST'],
        os.environ['REDIS_PORT']
    )
CACHES = {
    'site': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url,),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'default': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/1' % (url,),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# AWS
AWS_LAMBDA_FUNCTION_NAME = 'punchkicktap-production'
AWS_DEFAULT_ACL = 'private'
AWS_S3_SECURE_URLS = True
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_CLOUDFRONT_DOMAIN = os.environ['AWS_CLOUDFRONT_DOMAIN']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_S3_REGION_NAME = 'us-east-1'
AWS_AUTO_CREATE_BUCKET = False
AWS_S3_CUSTOM_DOMAIN = AWS_CLOUDFRONT_DOMAIN
S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN
AWS_SQS_IS_ENABLED = ast.literal_eval(os.environ.get('AWS_SQS_IS_ENABLED', 'False').capitalize())

# Media
DEFAULT_FILE_STORAGE = os.environ['DEFAULT_FILE_STORAGE']
if IS_TEST_MODE or DEFAULT_FILE_STORAGE == 'django.core.files.storage.FileSystemStorage':
    MEDIA_URL = '/media/'
    MEDIA_ROOT = '/media'
    if not os.path.exists(MEDIA_ROOT):
        os.makedirs(MEDIA_ROOT)
else:
    MEDIAFILES_LOCATION = 'media'
    MEDIA_URL = '%s/%s/' % (S3_URL, MEDIAFILES_LOCATION)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
# STATICFILES_DIRS = [os.path.join(BASE_DIR, 'punchkicktap/static/dist/')]
STATICFILES_STORAGE = os.environ['STATICFILES_STORAGE']
if IS_TEST_MODE or STATICFILES_STORAGE == 'django.contrib.staticfiles.storage.StaticFilesStorage':
    STATIC_URL = '/static/'
    STATIC_ROOT = '/static'
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)
else:
    STATICFILES_LOCATION = os.environ['STATICFILES_LOCATION']
    STATIC_URL = '%s/%s/' % (S3_URL, STATICFILES_LOCATION)

# # GeoDjango

geo_dir = os.path.dirname(os.environ['GEOS_LIBRARY_PATH'])
file_paths = [
    (os.path.join(geo_dir, 'libgeos-3.4.2.so'), os.path.join(geo_dir, 'libgeos.so')),
    (os.path.join(geo_dir, 'libgeos_c.so.1.8.2'), os.path.join(geo_dir, 'libgeos_c.so.1')),
    (os.path.join(geo_dir, 'libgeos_c.so.1'), os.path.join(geo_dir, 'libgeos_c.so')),
]
for src, dest in file_paths:
    if not os.path.exists(dest):
        os.symlink(src, dest)
GDAL_LIBRARY_PATH = os.environ['GDAL_LIBRARY_PATH']
GEOS_LIBRARY_PATH = os.environ['GEOS_LIBRARY_PATH']
# GDAL_LIBRARY_PATH = '/tmp/punchkicktap/lib/libgdal.so'
# GEOS_LIBRARY_PATH = '/tmp/punchkicktap/lib/libgeos_c.so'

# Which site...?
SITE_ID = 1
SITE_DOMAIN = os.environ['SITE_DOMAIN']  # 'localhost:5031'
SITE_NAME = os.environ['SITE_NAME']  # 'SHOTZU'
SITE_DISPLAY_NAME = os.environ['SITE_DISPLAY_NAME']  # 'SHOTZU'
WAGTAIL_SITE_NAME = SITE_NAME

# All auth
ACCOUNT_ADAPTER = 'punchkicktap.users.adapters.UserAdapter'
SOCIALACCOUNT_ADAPTER = 'punchkicktap.users.adapters.SocialAccountAdapter'
REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'punchkicktap.users.serializers.RegistrationSerializer',
}
REST_AUTH_SERIALIZERS = {
    'LOGIN_SERIALIZER': 'punchkicktap.users.serializers.LoginSerializer',
    'PASSWORD_RESET_SERIALIZER': 'punchkicktap.users.serializers.PasswordResetSerializer'
}
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'screen_name'
ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email_address'
ACCOUNT_EMAIL_SUBJECT_PREFIX = ''
ACCOUNT_PRESERVE_USERNAME_CASING = False
ACCOUNT_EMAIL_VERIFICATION = 'optional'

# Sessions
SESSION_COOKIE_SECURE = not DEBUG

# Email
ADMINS = [
    ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
    ('Chukwuemeka Ezekwe', 'emeka@punchkicktap.com')
]
EMAIL_BACKEND = os.environ['EMAIL_BACKEND']
SERVER_EMAIL = 'Bot <bot@%s>' % SITE_DOMAIN
DEFAULT_FROM_EMAIL = 'PunchKickTap <hello@%s>' % SITE_DOMAIN
SUPPORT_EMAIL = 'support@%s' % SITE_DOMAIN
INFO_EMAIL = 'info@%s' % SITE_DOMAIN
EMAIL_CLOSING = 'Peace,\nPunchKickTap'
# STAFF_EMAILS = [staff_email[1] for staff_email in STAFF]

# Logging
# 'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
LOG_LEVEL = os.getenv('DJANGO_LOG_LEVEL', 'INFO')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_is_test_mode_false': {
            '()': 'punchkicktap.log.RequireIsTestModeFalse',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'filters': ['require_is_test_mode_false'],
        }
    },
    'loggers': {
        '': {
            'handlers': ['console', 'mail_admins'],
            'level': LOG_LEVEL,
            'propagate': False
        },
        'django': {
            'handlers': ['console', 'mail_admins'],
            'level': LOG_LEVEL,
            'propagate': False
        },
        'django.request': {
            'handlers': ['console', 'mail_admins'],
            'level': LOG_LEVEL,
            'propagate': False
        },
        'punchkicktap': {
            'handlers': ['console', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': False
        },
    },
}

# Stripe
STRIPE_PUBLIC_KEY = os.environ.get('STRIPE_PUBLIC_KEY')
STRIPE_SECRET_KEY = os.environ.get('STRIPE_SECRET_KEY')

# For Zappa
DO_REMOVE_WSGI_SCRIPT_ALIAS = ast.literal_eval(os.environ.get('DO_REMOVE_WSGI_SCRIPT_ALIAS', 'False').capitalize())
WSGI_SCRIPT_ALIAS = os.environ.get('WSGI_SCRIPT_ALIAS', None)
# if DO_REMOVE_WSGI_SCRIPT_ALIAS:
#     FORCE_SCRIPT_NAME = WSGI_SCRIPT_ALIAS

# Minify
HTML_MINIFY = not IS_TEST_MODE and not DEBUG

# CSRF
CSRF_TRUSTED_ORIGINS = ALLOWED_HOSTS
USE_X_FORWARDED_HOST = True
