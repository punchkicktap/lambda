from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory
from django.conf import settings
from django.contrib.sites.models import Site

SITE_ID = getattr(settings, 'SITE_ID')
FACEBOOK_PUBLIC_KEY = getattr(settings, 'FACEBOOK_PUBLIC_KEY')
FACEBOOK_SECRET_KEY = getattr(settings, 'FACEBOOK_SECRET_KEY')


class SiteFactory(factory.django.DjangoModelFactory):
    domain = 'test.punchkicktap.com'
    name = 'Errbody'
    id = SITE_ID

    class Meta:
        model = Site
