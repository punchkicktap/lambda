module.exports = {
    npm: {
        globals: {jQuery: 'jquery'}
    },
    files: {
        javascripts: {
            joinTo: {
                'vendor.js': /^(?!app)/,
                'app.js': /^app/
            }
        },
        stylesheets: {
            joinTo: 'app.css'
        }
    },
    paths: {
        public: '/dist'
    },
    plugins: {
        babel: {
            presets: ['env']
        },
        sass: {
            mode: 'native'
        },
        autoReload: {
            port: 9485
        },
        uncss: {
            urls: [
                'http://web',
                'http://web/blog',
                'http://web/blog/im-pickle-rick/',
                'http://web/privacy',
                'http://web/terms'
            ],
            options: {
				ignore: [
					'blockquote',
					'.blockquote',
					'.blockquote-footer',
                    '.float-right',
                    '.float-left'
				]
			}
        },
        cssnano: {
            preset: ['default', {
                autoprefixer: true
            }]
        },
        // critical: {
        //     jobs: [
        //         {
        //             src: 'http://web',
        //             dest: '/src/app/css/above_the_fold/_lead_creation_form.scss',
        //             css: ['/dist/app.css'],
        //             folder: '/tmp',
        //             dimensions: [
        //                 {
        //                     height: 900,
        //                     width: 1300
        //                 },
        //                 {
        //                     height: 900,
        //                     width: 600
        //                 }
        //             ]
        //         },
        //         {
        //             src: 'http://web/blog',
        //             dest: '/src/app/css/above_the_fold/_blog_index.scss',
        //             css: ['/dist/app.css'],
        //             folder: '/tmp',
        //             dimensions: [
        //                 {
        //                     height: 900,
        //                     width: 1300
        //                 },
        //                 {
        //                     height: 900,
        //                     width: 600
        //                 }
        //             ]
        //         },
        //         {
        //             src: 'http://web/blog/im-pickle-rick/',
        //             dest: '/src/app/css/above_the_fold/_blog_page.scss',
        //             css: ['/dist/app.css'],
        //             folder: '/tmp',
        //             dimensions: [
        //                 {
        //                     height: 900,
        //                     width: 1300
        //                 },
        //                 {
        //                     height: 900,
        //                     width: 600
        //                 }
        //             ]
        //         }
        //     ]
        // }

    },
    watcher: {
        usePolling: true
    }

};