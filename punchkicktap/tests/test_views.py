from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import skip

from django.core.urlresolvers import reverse
from django_webtest import WebTest
from rest_framework import status

from punchkicktap.enums import View as ViewName


class TermsOfServiceViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse(ViewName.TERMS_OF_SERVICE.value)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'terms_of_service.html')

    def test_page_is_cached(self):
        url = reverse('terms_of_service')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')


