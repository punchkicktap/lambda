from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from punchkicktap.leads.forms import LeadForm
from punchkicktap.leads.models import Lead
from punchkicktap.leads.factories import LeadFactory


class LeadFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'full_name': 'Sterling Archer',
            'business_name': 'Danger Zone R\' Us',
            'email_address': 'sterlingarcher@gmail.com'
        }

    def tearDown(self):
        Lead.objects.all().delete()

    def test_create_lead(self):
        data = self.data.copy()
        form = LeadForm(data=data)
        self.assertTrue(form.is_valid())
        lead = form.save()
        self.assertIsNotNone(lead)
        self.assertEqual(Lead.objects.all().count(), 1)

    def test_existing_email_address(self):
        data = self.data.copy()
        LeadFactory(email_address=data.get('email_address'))
        form = LeadForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors.as_data())
        self.assertEqual(Lead.objects.all().count(), 1)

    def test_no_business_name(self):
        data = self.data.copy()
        data.pop('business_name')
        form = LeadForm(data=data)
        self.assertTrue(form.is_valid())
        lead = form.save()
        self.assertIsNotNone(lead)
        self.assertEqual(Lead.objects.all().count(), 1)
