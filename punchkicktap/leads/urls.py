from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import url

from punchkicktap.leads.enums import View as ViewName
from punchkicktap.leads.views import LeadCreationSuccessView
from punchkicktap.leads.views import LeadCreationView

urlpatterns = [
    url(r'^success/$', LeadCreationSuccessView.as_view(), name=ViewName.CREATE_LEAD_SUCCESS.value),
    url(r'^$', LeadCreationView.as_view(), name=ViewName.CREATE_LEAD.value),
]
