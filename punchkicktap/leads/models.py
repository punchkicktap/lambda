from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Lead(AbstractModel):
    full_name = models.CharField(
        _('full_name'),
        blank=False,
        max_length=256,
    )
    business_name = models.CharField(
        _('business_name'),
        blank=True,
        max_length=256,
        default=''
    )
    email_address = models.EmailField(
        _("email address"),
        blank=False,
        max_length=256,
        unique=True,
        error_messages={
            'unique': 'This email address has already been registered.'
        },
    )

    def __str__(self):
        return self.full_name
