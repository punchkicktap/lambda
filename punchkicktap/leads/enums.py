from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum


class View(Enum):
    CREATE_LEAD = 'create_lead'
    CREATE_LEAD_SUCCESS = 'create_lead_success'
