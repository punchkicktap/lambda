from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.forms import ModelForm

from punchkicktap.leads.models import Lead


class LeadForm(ModelForm):
    class Meta:
        model = Lead
        fields = [
            'full_name',
            'business_name',
            'email_address'
        ]
