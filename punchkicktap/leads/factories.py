from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory

from punchkicktap.leads.models import Lead


class LeadFactory(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    business_name = factory.Faker('company')
    email_address = factory.Faker('email')

    class Meta:
        model = Lead
