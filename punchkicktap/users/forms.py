from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django import forms
from django.contrib.auth import forms as auth_forms
from django.contrib.auth import get_user_model

User = get_user_model()


class AuthenticationForm(auth_forms.AuthenticationForm):
    username = forms.EmailField()

    def add_error(self, field, error):
        if field == 'username':
            error = forms.ValidationError('That doesn\'t look like a valid email address. Please try entering another one.')
        super(AuthenticationForm, self).add_error(field, error)
