import json
import logging
import uuid
from importlib import import_module
from time import sleep

import random
import requests
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator
from django.core.validators import MinLengthValidator
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_delete
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from zxcvbn import zxcvbn

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
SCREEN_NAME_VALIDATORS = [
    MinLengthValidator(1),
    MaxLengthValidator(32, message='Please pick a name that is less than 32 characters.'),
    RegexValidator('^[\w]+$', inverse_match=False)
]
logger = logging.getLogger(__name__)


class UserManager(BaseUserManager):
    def _create_user(self, email_address, password, is_staff, is_superuser, **extra_fields):
        if not email_address:
            raise ValueError("The given email must be set")
        user = self.model(email_address=self.normalize_email(email_address),
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=timezone.now(),
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email_address, password=None, **extra_fields):
        return self._create_user(email_address, password, False, False, **extra_fields)

    def create_superuser(self, email_address, password, **extra_fields):
        return self._create_user(email_address, password, True, True, **extra_fields)


# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
    email_address = models.EmailField(
        _("email address"),
        blank=False,
        max_length=256,
        unique=True,
        error_messages={
            'unique': 'That email address is already linked to another account. Please try entering another one.',
            'invalid': 'That doesn\'t look like a valid email address. Please try entering another one.',
        },
    )
    password = models.CharField(
        _('password'),
        max_length=128,
        error_messages={
            'invalid': 'Please choose a stronger password.',
            'min_length': 'Please use a password that is at least 6 characters.',
            'max_length': 'I appreciate your passion for security. Unfortunately, you must use a password that is less than 128 characters',
        },
        validators=[MinLengthValidator(6)]
    )
    # nickname = models.CharField(_("nickname"), max_length=256, blank=True)
    full_name = models.CharField(_("full name"), max_length=256, blank=True)
    # messenger_user_id = models.CharField(_("Messenger user id"), max_length=256, blank=True, default='')
    # cognito_identity_id = models.CharField(_("cognito identity id"), max_length=256, blank=True, default='')
    # profile_image = models.ImageField(upload_to='users/users/profile_images/', null=True)
    # bio = models.CharField(_("bio"), max_length=255, blank=True, default='')

    is_staff = models.BooleanField(_("staff status"), default=False, help_text=_("Designates whether the user can log into this admin site."))
    is_active = models.BooleanField(_("active"), default=True, help_text=_("Designates whether this user should be treated as active. Unselect this instead of deleting accounts."))
    date_joined = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email_address'

    objects = UserManager()

    def get_full_name(self):
        return self.full_name

    def get_short_name(self):
        return self.full_name

    def clean(self):
        # Check password strength
        if self.password:
            user_inputs = []
            for key in ['email_address', 'nickname', 'full_name']:
                if hasattr(self, key) and getattr(self, key):
                    user_inputs.append(getattr(self, key))
            results = zxcvbn(self.password, user_inputs=user_inputs)
            if results.get('score') < 2:
                raise ValidationError(message={'password': ['Please choose a stronger password.']}, code='invalid')
