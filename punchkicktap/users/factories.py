from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from uuid import uuid4

import factory
import random
from faker import Faker
import random

from punchkicktap.users.models import User
from django.utils.lorem_ipsum import paragraphs, words
import requests
from django.core.files.base import ContentFile

DEFAULT_PASSWORD = 'over9000'


class UserFactory(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    password = factory.PostGenerationMethodCall('set_password', DEFAULT_PASSWORD)

    @factory.lazy_attribute
    def bio(self):
        return words(random.randint(5, 15), common=False)
    
    @factory.lazy_attribute
    def messenger_user_id(self):
        return uuid4().hex

    @factory.lazy_attribute
    def email_address(self):
        faker = Faker()
        email_address = None
        while email_address is None or User.objects.filter(email_address=email_address).exists():
            email_address = faker.email()
        return email_address

    @factory.post_generation
    def post(obj, create, extracted, **kwargs):
        if kwargs.get('do_create_profile_image', False):
            response = requests.get('http://lorempixel.com/100/100/')
            obj.profile_image.save(uuid4().hex, ContentFile(response.content))
    
        
    class Meta:
        model = User
