from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = list()

# Admin
urlpatterns.extend([
    url(r'^fbd0f2fc/', include(admin.site.urls)),
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot'))
])
