from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum


class View(Enum):
    MESSENGER_WEBHOOK = 'messenger_webhook'
    TERMS_OF_SERVICE = 'terms_of_service'
    PRIVACY_POLICY = 'privacy_policy'


class Namespace(Enum):
    LEX_API = 'api_lex'
    BUSINESSES_API = 'api_businesses'
    BUSINESSES = 'businesses'
    LEADS = 'leads'
