from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rest_framework import pagination
from django.core.paginator import Paginator as DjangoPaginator
import pickle
from django.core import signing
from importlib import import_module
from base64 import b64encode
from base64 import b64decode
from uuid import uuid4
from django.core.cache import cache
import logging

logger = logging.getLogger(__name__)


class Paginator(DjangoPaginator):
    @staticmethod
    def from_page_token(token):
        key = signing.loads(token)
        data = cache.get(key)
        try:
            assert data
        except AssertionError:
            logger.exception('Failed to find %s in cache' % token)

        module_name = '.'.join(data.get('model_path').split('.')[:-1])
        model_name = data.get('model_path').split('.')[-1]
        model = getattr(import_module(module_name), model_name)
        queryset = model.objects.all()
        queryset.query = pickle.loads(b64decode(data.get('query')))
        paginator = Paginator(
            object_list=queryset,
            per_page=data.get('page_size')
        )
        page = paginator.page(data.get('page_number'))
        return paginator, page

    def get_page_token(self, page_number, model_path):
        if page_number not in self.page_range:
            return None
        key = uuid4().hex
        payload = {
            'page_number': page_number,
            'page_size': self.per_page,
            'model_path': model_path,
            'query': b64encode(pickle.dumps(self.object_list.query)).decode(),
        }
        cache.set(key, payload, 60 * 60 * 24 * 5)
        return signing.dumps(key)


class LargeLimitOffsetPagination(pagination.LimitOffsetPagination):
    default_limit = 100
    max_limit = 200


class LimitOffsetPagination(pagination.LimitOffsetPagination):
    default_limit = 100
    max_limit = 200
