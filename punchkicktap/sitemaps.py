from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib import sitemaps
from django.urls import reverse
from punchkicktap.enums import View as ViewName

class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return [
            ViewName.TERMS_OF_SERVICE.value, 
            ViewName.PRIVACY_POLICY.value
        ]

    def location(self, item):
        return reverse(item)