from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import json as json

import base64
import boto3

argument_parser = argparse.ArgumentParser(description='Invoke Lambda function')
argument_parser.add_argument('function_name', type=str, help='Name of Lambda function')
argument_parser.add_argument('command', type=str, help='Command to run')
args = argument_parser.parse_args()

payload = {
    'manage': args.command
}
lambda_client = boto3.client('lambda', region_name='us-east-1')
response = lambda_client.invoke(
    FunctionName=args.function_name,
    InvocationType='RequestResponse',
    LogType='Tail',
    Payload=json.dumps(payload)
)
if 'LogResult' in response:
    print(str(base64.b64decode(response['LogResult'])))
else:
    print(response)
