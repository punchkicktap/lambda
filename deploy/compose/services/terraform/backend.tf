terraform {
  backend "s3" {
    bucket = "punchkicktap-production"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}