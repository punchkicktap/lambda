resource "aws_lambda_permission" "cloudwatch" {
  statement_id   = "AllowExecutionFromCloudWatch"
  action         = "lambda:InvokeFunction"
  function_name  = "${aws_lambda_function.default.function_name}"
  principal      = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.keep_warm.arn}"
}

resource "aws_cloudwatch_event_rule" "keep_warm" {
  name        = "${var.app_name}-${var.environment}-keep-warm-handler.keep_warm_callback"
  description = "Keep Lambda function warm"
  schedule_expression = "rate(10 minutes)"
  is_enabled = "true"
}

resource "aws_cloudwatch_event_target" "keep_warm" {
  rule      = "${aws_cloudwatch_event_rule.keep_warm.name}"
  arn       = "${aws_lambda_function.default.arn}"

  depends_on = [
    "aws_lambda_permission.cloudwatch"
  ]

}
