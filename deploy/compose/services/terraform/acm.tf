data "aws_acm_certificate" "default" {
  domain   = "${var.site_domain}"
  statuses = ["ISSUED"]
}