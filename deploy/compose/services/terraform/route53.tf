resource "aws_ses_domain_identity" "default" {
  domain = "${var.site_domain}"
}

resource "aws_route53_zone" "default" {
  name = "${var.site_domain}"
}

//resource "aws_route53_record" "cdn_alias" {
//  zone_id = "${aws_route53_zone.default.id}"
//  name = "cdn"
//  type = "A"
//  alias {
//    name = "${aws_cloudfront_distribution.default.domain_name}"
//    zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
//    evaluate_target_health = false
//  }
//}

resource "aws_route53_record" "www_alias" {
  zone_id = "${aws_route53_zone.default.id}"
  name = "www"
  type = "CNAME"
  ttl = "300"
  records = ["${var.site_domain}"]
//  alias {
//    name = "${aws_cloudfront_distribution.default.domain_name}"
//    zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
//    evaluate_target_health = false
//  }
}

resource "aws_route53_record" "root_alias" {
  zone_id = "${aws_route53_zone.default.id}"
  name = ""
  type = "A"
  alias {
    name = "${aws_cloudfront_distribution.default.domain_name}"
    zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "ses_txt" {
  zone_id = "${aws_route53_zone.default.id}"
  name    = "_amazonses.${var.site_domain}"
  type    = "TXT"
  ttl     = "600"
  records = ["${aws_ses_domain_identity.default.verification_token}"]
}

resource "aws_route53_record" "cname" {
  count = "${length(var.cname_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.cname_records), count.index)}"
  type    = "CNAME"
  ttl     = "600"
  records = ["${lookup(var.cname_records, element(keys(var.cname_records), count.index))}"]
}

resource "aws_route53_record" "mx" {
  count = "${length(var.mx_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.mx_records), count.index)}"
  type    = "MX"
  ttl     = "600"
  records = ["${lookup(var.mx_records, element(keys(var.mx_records), count.index))}"]
}


output "route53_zone_name_servers" {
  value = "${aws_route53_zone.default.name_servers}"
}
