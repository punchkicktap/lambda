resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = false
  enable_dns_support = true
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

/*
  Private Subnets
*/
resource "aws_subnet" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${element(var.private_subnet_cidrs, count.index)}"
  availability_zone = "${element(var.aws_private_availability_zones, count.index)}"
  tags {
    Name = "${var.app_name}-${var.environment}-${element(var.aws_private_availability_zones, count.index)}-private"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}
resource "aws_route_table" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    instance_id = "${aws_instance.nat_gateway.id}"
  }

  tags {
    Name = "Private Subnet"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table_association" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  subnet_id = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

/*
  NAT Instance
*/
resource "aws_security_group" "nat_gateway" {
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = "${var.private_subnet_cidrs}"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = "${var.private_subnet_cidrs}"
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    egress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.app_name}-${var.environment}"
		Environment = "${var.environment}"
		Project = "${var.project_name}"
    }
}

resource "aws_eip" "nat_gateway" {
  instance = "${aws_instance.nat_gateway.id}"
  vpc = true
}

resource "aws_instance" "nat_gateway" {
    ami = "ami-a7fdcadc" # this is a special ami preconfigured to do NAT
    availability_zone = "${var.aws_availability_zone}"
    instance_type = "t2.nano"
    vpc_security_group_ids = ["${aws_security_group.nat_gateway.id}"]
    subnet_id = "${aws_subnet.public.id}"
    associate_public_ip_address = true
    source_dest_check = false

    tags {
        Name = "${var.app_name}-${var.environment}-nat_gateway"
		Environment = "${var.environment}"
		Project = "${var.project_name}"
    }
}

# Public Internet
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "${var.aws_availability_zone}"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.app_name}-${var.environment}-public"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public.id}"
}
