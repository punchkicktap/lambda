variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "django_secret_key" {}
variable "stripe_public_key" {}
variable "stripe_secret_key" {}
variable "environment" {}
variable "debug" {}

variable "aws_region" {
  default = "us-east-1"
}
variable "aws_availability_zone" {
  default = "us-east-1a"
}
variable "aws_private_availability_zones" {
  type = "list"
  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c"
  ]
}

variable "cloudfront_domain" {

}
variable "site_domain" {
}
variable "site_name" {
}
variable "site_display_name" {
}
variable "api_name" {
  default = "PunchKickTap"
}
variable "app_name" {
  default = "punchkicktap"
}
variable "project_name" {
  default = "PunchKickTap"
}
variable "s3_bucket_name" {}


variable "cache_node_type" {
  default = "cache.t2.micro"
}
variable "cache_port" {
  default = 6379
}
variable "cache_engine" {
  default = "redis"
}
variable "cache_cluster_id" {
  default = "punchkicktap"
}
variable "cache_num_cache_nodes" {
  default = 1
}
variable "cache_parameter_group_name" {
  default = "default.redis3.2"
}

variable "database_user" {
  default = "postgres"
}
variable "database_password" {
  default = "c38bab650dcb46c59e0541fa0bc23b96"
}
variable "database_instance_class" {
  default = "db.t2.micro"
}
variable "database_engine" {
  default = "postgres"
}
variable "database_allocated_storage" {
  default = 20
}
variable "database_storage_type" {
  default = "gp2"
}
variable "database_port" {
  default = 5432
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default = "10.0.0.0/24"
}
variable "private_subnet_cidrs" {
  description = "CIDR for the Private Subnet"
  type = "list"
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
  ]
}

variable "lambda_runtime" {
  default = "python3.6"
}
variable "lambda_handler" {
  default = "handler.lambda_handler"
}
variable "lambda_memory_size" {
  default = 1024
}
variable "lambda_timeout" {
  default = 300
}
variable "lambda_handler_s3_key" {
  default = "zappa/handler.zip"
}
variable "lambda_project_s3_key" {
  default = "zappa/punchkicktap_current_project.zip"
}
variable "lambda_handler_file_path" {
  default = "zappa/handler.zip"
}
variable "lambda_project_file_path" {
  default = "zappa/punchkicktap_current_project.zip"
}
variable "lambda_ld_library_path" {
  default = "/var/lang/lib:/lib64:/usr/lib64:/var/runtime:/var/runtime/lib:/var/task:/var/task/lib:/tmp/punchkicktap/lib"
}
variable "lambda_gdal_library_path" {
  default = "/tmp/punchkicktap/lib/libgdal.so"
}
variable "lambda_geos_library_path" {
  default = "/tmp/punchkicktap/lib/libgeos_c.so"
}
variable "lambda_python_path" {
  default = "/var/runtime:/tmp/punchkicktap:/tmp/punchkicktap/punchkicktap:/tmp/punchkicktap/punchkicktap/punchkicktap"
}
variable "lambda_default_file_storage" {
  default = "punchkicktap.storages.S3MediaStorage"
}
variable "lambda_staticfiles_storage" {
  default = "punchkicktap.storages.S3StaticStorage"
}
variable "lambda_cache_backend" {
  default = "django_redis.cache.RedisCache"
}
variable "lambda_email_backend" {
  default = "django_amazon_ses.backends.boto.EmailBackend"
}
variable "lambda_staticfiles_location" {}
variable "lambda_mediafiles_location" {
  default = "media"
}
variable "lambda_do_remove_wsgi_script_alias" {
  default = "True"
}
variable "lambda_wsgi_script_alias" {
  default = "/production"
}

variable "cname_records" {
  type = "map"
  default = {
    "qi2a5mxne2ayzfgaankxse2qbcurzrl2._domainkey.punckkicktap.com" = "qi2a5mxne2ayzfgaankxse2qbcurzrl2.dkim.amazonses.com"
    "3jt7ovc6qqsp2mdmou4cc76itn7dxc36._domainkey.punckkicktap.com" = "3jt7ovc6qqsp2mdmou4cc76itn7dxc36.dkim.amazonses.com"
    "wl626bm6edwnn7ehshnkhwozpo7veqmu._domainkey.punckkicktap.com" = "wl626bm6edwnn7ehshnkhwozpo7veqmu.dkim.amazonses.com"
    "autodiscover.punckkicktap.com" = "autodiscover.mail.us-east-1.awsapps.com"
  }
}

variable "mx_records" {
  type = "map"
  default = {
    "punchkicktap.com" = "10 inbound-smtp.us-east-1.amazonaws.com"
  }
}

variable "sqs_is_enabled" {
  default = "True"
}